BEGIN PLOT /LHCB_2016_I1414195/d01-x01-y01
Title=Helicity angle for $D_{s1}(2536)^+$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
YMin=0
END PLOT
BEGIN PLOT /LHCB_2016_I1414195/d01-x01-y02
Title=Helicity angle for $D_{s2}^*(2573)^+$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
YMin=0
END PLOT
BEGIN PLOT /LHCB_2016_I1414195/d02-x01-y01
Title=Helicity angle for $D^*_{s1}(2700)^+$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
YMin=0
END PLOT
BEGIN PLOT /LHCB_2016_I1414195/d02-x01-y02
Title=Helicity angle for $D_{sJ}^*(2860)^+$ assuming $D_{s1}^*(2860)^+$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
YMin=0
END PLOT
BEGIN PLOT /LHCB_2016_I1414195/d02-x01-y04
Title=Helicity angle for $D_{sJ}^*(2860)^+$ assuming $D_{s3}^*(2860)^+$
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
YMin=0
END PLOT
BEGIN PLOT /LHCB_2016_I1414195/d02-x01-y05
Title=Helicity angle for $D_{sJ}^*(2860)^+$  $D_{s1}^*(2860)^+$/$D_{s3}^*(2860)^+$ admixture
XLabel=$\cos\theta_H$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_H$
LogY=0
YMin=0
END PLOT
