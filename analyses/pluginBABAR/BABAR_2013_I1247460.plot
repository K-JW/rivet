BEGIN PLOT /BABAR_2013_I1247460/d01-x01-y01
Title=$B^+\to \omega\ell^+\nu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$10^{5} \mathrm{d}\mathrm{Br}(B^+\to\omega\ell^+\nu)/\mathrm{d}q^2$ [GeV$^{-2}$]
LogY=0
END PLOT
