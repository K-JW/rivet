BEGIN PLOT /BABAR_2016_I1391152
XLabel=$q^2$ [$\text{GeV}^2$]
LogY=0
END PLOT


BEGIN PLOT /BABAR_2016_I1391152/d01-x0[1,2]-y01
Title=Longitudinal $K^*$ polarization in $B^+\to K^{*+}\ell^+\ell^-$
YLabel=$F_L$
END PLOT
BEGIN PLOT /BABAR_2016_I1391152/d01-x0[1,2]-y02
Title=Longitudinal $K^*$ polarization in $B^0\to K^{*0}\ell^+\ell^-$
YLabel=$F_L$
END PLOT
BEGIN PLOT /BABAR_2016_I1391152/d01-x0[1,2]-y03
Title=Longitudinal $K^*$ polarization in $B\to K^*\ell^+\ell^-$
YLabel=$F_L$
END PLOT

BEGIN PLOT /BABAR_2016_I1391152/d02-x0[1,2]-y01
Title=Forward-Backward asymmetry in $B^+\to K^{*+}\ell^+\ell^-$
YLabel=$A_{FB}$
END PLOT
BEGIN PLOT /BABAR_2016_I1391152/d02-x0[1,2]-y02
Title=Forward-Backward asymmetry in $B^0\to K^{*0}\ell^+\ell^-$
YLabel=$A_{FB}$
END PLOT
BEGIN PLOT /BABAR_2016_I1391152/d02-x0[1,2]-y03
Title=Forward-Backward asymmetry in $B\to K^*\ell^+\ell^-$
YLabel=$A_{FB}$
END PLOT

BEGIN PLOT /BABAR_2016_I1391152/d03-x0[1,2]-y01
Title=$P_2$ in $B^+\to K^{*+}\ell^+\ell^-$
YLabel=$P_2$
END PLOT
BEGIN PLOT /BABAR_2016_I1391152/d03-x0[1,2]-y02
Title=$P_2$ in $B^0\to K^{*0}\ell^+\ell^-$
YLabel=$P_2$
END PLOT
BEGIN PLOT /BABAR_2016_I1391152/d03-x0[1,2]-y03
Title=$P_2$ in $B\to K^*\ell^+\ell^-$
YLabel=$P_2$
END PLOT
