BEGIN PLOT /BABAR_2016_I1487722/d01-x01-y01
Title=Mass of pion system in $B^0\to D^{*-}\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
