BEGIN PLOT /BABAR_2010_I850492/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $\Upsilon_2(1D)\to \pi^+\pi^- \Upsilon(1S)$
XLabel=$m_{\pi^+\pi^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I850492/d01-x01-y02
Title=$\chi$ in $\Upsilon_2(1D)\to \pi^+\pi^- \Upsilon(1S)$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2010_I850492/d01-x01-y03
Title=$\cos\theta_\pi$ in $\Upsilon_2(1D)\to \pi^+\pi^- \Upsilon(1S)$
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_\pi$ 
LogY=0
END PLOT
