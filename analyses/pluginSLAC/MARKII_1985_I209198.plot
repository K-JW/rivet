BEGIN PLOT /MARKII_1985_I209198/d02-x01-y01
Title=$\Lambda^0$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mathrm{nb} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /MARKII_1985_I209198/d03-x01-y01
Title=$\Lambda^0$ $p_\per$ w.r.t the thrust axis at 29 GeV
XLabel=$p_\perp^2$ [$\mathrm{GeV}^2$]
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}p_\perp^2$ [$\mathrm{GeV}^{-2}$]
END PLOT