BEGIN PLOT /MARKIII_1987_I247266/
LogY=0
END PLOT

BEGIN PLOT /MARKIII_1987_I247266/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d01-x01-y02
Title=$\pi^0\pi^+$ mass distribution in $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{\pi^0\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d01-x01-y03
Title=$K^-\pi^0$ mass distribution in $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/dalitz1
Title=Dalitz plot for $D^0\to K^-\pi^+\pi^0$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{\pi^+\pi^0}$ [$\rm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MARKIII_1987_I247266/d02-x01-y01
Title=$\bar{K}^0\pi^+$ mass distribution in $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\bar{K}^0\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\bar{K}^0\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d02-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d02-x01-y03
Title=$\bar{K}^0\pi^-$ mass distribution in $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\bar{K}^0\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\bar{K}^0\pi^-}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/dalitz2
Title=Dalitz plot for $D^0\to \bar{K}^0\pi^+\pi^-$
XLabel=$m^2_{\bar{K}^0\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\bar{K}^0\pi^+}/{\rm d}m^2_{\pi^+\pi^-}$ [$\rm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MARKIII_1987_I247266/d03-x01-y02
Title=$\pi^+\pi^0$ mass distribution in $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d03-x01-y03
Title=$K^0_S\pi^0$ mass distribution in $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d03-x01-y01
Title=$K^0_S\pi^+$ mass distribution in $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/dalitz3
Title=Dalitz plot for $D^+\to K_S^0\pi^+\pi^0$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\K_S^0\pi^0}/{\rm d}m^2_{\pi^+\pi^0}$ [$\rm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MARKIII_1987_I247266/d04-x01-y02
Title=$\pi^+\pi^+$ mass distribution in $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{\pi^+\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d04-x01-y03
Title=Lower $K^-\pi^+$ mass distribution in $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/d04-x01-y01
Title=Higher $K^-\pi^+$ mass distribution in $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /MARKIII_1987_I247266/dalitz4
Title=Dalitz plot for $D^+\to K^-\pi^+\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{\pi^+\pi^+}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^-\pi^+}/{\rm d}m^2_{\pi^+\pi^+}$ [$\rm{GeV}^{-4}$]
END PLOT
