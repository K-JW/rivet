BEGIN PLOT /BESIII_2016_I1411448/d01-x01-y02
Title=$p\phi$ mass distribution in $J/\psi\to p\bar{p}\phi$ ($\phi\to K^0_SK^0_L$)
XLabel=$m_{p\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1411448/d01-x01-y01
Title=$p\bar{p}$ mass distribution in $J/\psi\to p\bar{p}\phi$ ($\phi\to K^0_SK^0_L$)
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1411448/d01-x01-y03
Title=$\bar{p}\phi$ mass distribution in $J/\psi\to p\bar{p}\phi$ ($\phi\to K^0_SK^0_L$)
XLabel=$m_{\bar{p}\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2016_I1411448/d01-x01-y05
Title=$p\phi$ mass distribution in $J/\psi\to p\bar{p}\phi$ ($\phi\to K^+K^-$)
XLabel=$m_{p\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1411448/d01-x01-y04
Title=$p\bar{p}$ mass distribution in $J/\psi\to p\bar{p}\phi$ ($\phi\to K^+K^-$)
XLabel=$m_{p\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1411448/d01-x01-y06
Title=$\bar{p}\phi$ mass distribution in $J/\psi\to p\bar{p}\phi$ ($\phi\to K^+K^-$)
XLabel=$m_{\bar{p}\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2016_I1411448/dalitz
Title=Dalitz plot for  $J/\psi\to p\bar{p}\phi$
XLabel=$m^2_{p\phi}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}\phi}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\phi}/{\rm d}m^2_{\bar{p}\phi}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
