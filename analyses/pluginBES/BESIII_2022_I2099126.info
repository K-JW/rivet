Name: BESIII_2022_I2099126
Year: 2022
Summary: Measurement of $\Lambda\to n\gamma$ decay asymmetry using  $J/\psi$ decays to $\Lambda^0\bar\Lambda^0$
Experiment: BESIII
Collider: BEPC
InspireID: 2099126
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2206.10791 [hep-ex]
Beams: [e-, e+]
Energies: [3.1]
Description:
  'Analysis of the angular distribution of the baryons, and decay products, produced in
   $e^+e^-\to J/\psi \to \Lambda^0\bar\Lambda^0$ with the decay $\Lambda\to n\gamma$.
   Gives information about the decay and is useful for testing correlations in hadron decays. N.B. the moment data is not corrected and should only be used qualatively.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022rgl
BibTeX: '@article{BESIII:2022rgl,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of the branching fraction and decay asymmetry of $\Lambda\to n\gamma$}",
    eprint = "2206.10791",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "6",
    year = "2022"
}
'
