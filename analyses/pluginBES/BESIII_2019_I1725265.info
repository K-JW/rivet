Name: BESIII_2019_I1725265
Year: 2019
Summary: Mass distributions in the decay $D^0\to K^-\pi^+\pi^0\pi^0$
Experiment: BESIII
Collider: BEPC
InspireID: 1725265
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 99 (2019) 9, 092008
RunInfo: Any process producing D0 mesons
Description:
  'Measurement of the mass distributions in the decay $D^0\to K^-\pi^+\pi^0\pi^0$ by BES. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2019lwn
BibTeX: '@article{BESIII:2019lwn,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Amplitude Analysis and Branching Fraction Measurement of $D^0\rightarrow K^-\pi^+\pi^0\pi^0$}",
    eprint = "1903.06316",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.99.092008",
    journal = "Phys. Rev. D",
    volume = "99",
    number = "9",
    pages = "092008",
    year = "2019"
}
'
