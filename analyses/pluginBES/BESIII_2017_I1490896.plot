BEGIN PLOT /BESIII_2017_I1490896/d01-x01-y01
Title=$\eta\pi^\pm$ mass distribution in $\chi_{c1}\to \eta\pi^+\pi^-$
XLabel=$m_{\eta\pi^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\pi^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1490896/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $\chi_{c1}\to \eta\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1490896/dalitz
Title=Dalitz plot for $\chi_{c1}\to \eta\pi^+\pi^-$
XLabel=$m^2_{\eta\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta\pi^+}/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-4}$]
LogY=0
END PLOT
