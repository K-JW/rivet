BEGIN PLOT /BESIII_2019_I1694530
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y01
Title=$\pi^-\pi^0$ mass in $D^0\to \pi^-\pi^0 e^+\nu_e$
XLabel=$m_{\pi^-\pi^0}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y02
Title=$e^+\nu_e$ mass squared in $D^0\to \pi^-\pi^0 e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y03
Title=$\cos\theta_e$ in $D^0\to \pi^-\pi^0 e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y04
Title=$\cos\theta_\pi$ in $D^0\to \pi^-\pi^0 e^+\nu_e$
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_\pi$ 
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y05
Title=$\chi$ in $D^0\to \pi^-\pi^0 e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT

BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y06
Title=$\pi^-\pi^0$ mass in $D^+\to \pi^+\pi^- e^+\nu_e$
XLabel=$m_{\pi^-\pi^0}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y07
Title=$e^+\nu_e$ mass squared in $D^+\to \pi^+\pi^- e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y08
Title=$\cos\theta_e$ in $D^+\to \pi^+\pi^- e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y09
Title=$\cos\theta_\pi$ in $D^+\to \pi^+\pi^- e^+\nu_e$
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_\pi$ 
END PLOT
BEGIN PLOT /BESIII_2019_I1694530/d01-x01-y10
Title=$\chi$ in $D^+\to \pi^+\pi^- e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
