BEGIN PLOT /BELLE_2021_I1809180/d01-x01-y01
Title=Helicity angle for $\Xi_c(2790)^+\to\Xi_c^{*0}\pi^+$
XLabel=$\cos\theta_h$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta_h$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1809180/d02-x01-y01
Title=Decay angle for $\Xi_c(2790)^+\to\Xi_c^{*0}(\to\Xi_c^+\pi^-)\pi^+$
XLabel=$\cos\theta_c$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta_c$
LogY=0
END PLOT
