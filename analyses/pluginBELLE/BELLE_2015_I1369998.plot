BEGIN PLOT /BELLE_2015_I1369998
LogY=0
END PLOT

BEGIN PLOT /BELLE_2015_I1369998/d01-x01-y01
Title= $\omega\pi$ mass squared distribution
XLabel=$m^2_{\omega\pi}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\omega\pi}$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d01-x01-y02
Title= $\cos\xi_1$ distribution
XLabel=$\cos\xi_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\xi_1$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d01-x01-y03
Title= $\cos\theta_1$ distribution
XLabel=$\cos\theta_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_1$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d01-x01-y04
Title= $\cos\beta_1$ distribution
XLabel=$\cos\beta_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\beta_1$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d01-x01-y05
Title= $\phi_1$ distribution
XLabel=$\phi_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_1$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d01-x01-y06
Title= $\psi_1$ distribution
XLabel=$\psi_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\psi_1$
END PLOT

BEGIN PLOT /BELLE_2015_I1369998/d02-x01-y01
Title= $D^*\pi$ mass squared distribution
XLabel=$m^2_{D^*\pi}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{D^*\pi}$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d02-x01-y02
Title= $\cos\xi_2$ distribution
XLabel=$\cos\xi_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\xi_2$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d02-x01-y03
Title= $\cos\theta_2$ distribution
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_2$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d02-x01-y04
Title= $\cos\beta_2$ distribution
XLabel=$\cos\beta_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\beta_2$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d02-x01-y05
Title= $\phi_2$ distribution
XLabel=$\phi_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_2$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d02-x01-y06
Title= $\psi_2$ distribution
XLabel=$\psi_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\psi_2$
END PLOT



BEGIN PLOT /BELLE_2015_I1369998/d03-x01-y01
Title= $\omega\pi$ mass squared distribution ($|\cos\theta_1|>0.5$)
XLabel=$m^2_{\omega\pi}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\omega\pi}$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d03-x01-y02
Title= $\omega\pi$ mass squared distribution ($|\cos\theta_1|<0.5$)
XLabel=$m^2_{\omega\pi}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\omega\pi}$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d03-x01-y03
Title= $\cos\beta_1$ distribution ($\cos\xi_2>-0.4$)
XLabel=$\cos\beta_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\beta_1$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d03-x01-y04
Title= $\cos\beta_1$ distribution ($|\cos\theta_1|<0.5$)
XLabel=$\cos\beta_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\beta_1$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d03-x01-y05
Title= $\psi_1$ distribution ($\cos\xi_2>-0.4$)
XLabel=$\psi_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\psi_1$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d03-x01-y06
Title= $\psi_1$ distribution ($|\cos\theta_1|<0.5$)
XLabel=$\psi_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\psi_1$
END PLOT

BEGIN PLOT /BELLE_2015_I1369998/d02-x01-y01
Title= $D^*\pi$ mass squared distribution ($\cos\xi_2>-0.4$)
XLabel=$m^2_{D^*\pi}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{D^*\pi}$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d04-x01-y02
Title= $D^*\pi$ mass squared distribution ($|\cos\theta_1|<0.5$)
XLabel=$m^2_{D^*\pi}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{D^*\pi}$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d04-x01-y03
Title= $\cos\theta_2$ distribution ($\cos\xi_2>-0.4$)
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_2$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d04-x01-y04
Title= $\cos\theta_2$ distribution ($|\cos\theta_1|<0.5$)
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_2$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d04-x01-y05
Title= $\psi_2$ distribution ($\cos\xi_2>-0.4$)
XLabel=$\psi_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\psi_2$
END PLOT
BEGIN PLOT /BELLE_2015_I1369998/d04-x01-y06
Title= $\psi_2$ distribution ($|\cos\theta_1|<0.5$)
XLabel=$\psi_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\psi_2$
END PLOT
